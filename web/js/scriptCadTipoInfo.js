const urlTipoInfoApi = "http://localhost:8080/tipoinformacao";
const boatoEnviar = document.querySelector("#enviar");
const nome = document.querySelector("#nome");
const descricao = document.querySelector("#descricao");

function enviaTipoInfo(){
    var tipoInfo = new Object();
    tipoInfo.nome = nome.value;
    tipoInfo.descricao = descricao.value;

	 fetch(urlTipoInfoApi, 
			 {
		 		method: 'POST',  
		 		headers: {
	 		      'Content-type': 'application/json'
	 		    },
		 		body: JSON.stringify(tipoInfo)
	 		})
	 .then(extrairJSON)
     .then(tratarResposta);
}

function tratarResposta(resposta){
    alert('Sucesso!');
    nome.value = "";
    descricao.value = "";
}


function extrairJSON(resposta){
    return resposta.json();
}

boatoEnviar.onclick = enviaTipoInfo;