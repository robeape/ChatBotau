const urlAreaApi = "http://localhost:8080/area";
const urlTipoInfoApi = "http://localhost:8080/tipoinformacao";
const urlAreaInfoApi = "http://localhost:8080/areainformacao";
const boatoEnviar = document.querySelector("#enviar");
const comboArea = document.querySelector("#area");
const comboTipoInfo = document.querySelector("#tipoInfo");
const descricao = document.querySelector("#descricao");



function carregaArea(){
    fetch(urlAreaApi)
    .then(extrairJSON)
    .then(carregaComboArea);
}

function carregaTipoInfo(){
    fetch(urlTipoInfoApi)
    .then(extrairJSON)
    .then(carregaComboTipoInfo);
}



function carregaComboArea(areas){
    
    areas.forEach(area => {
        let option = document.createElement("option");
    option.value = area.nome;
    option.innerHTML = area.nome;

    comboArea.appendChild(option);
    });
}

function carregaComboTipoInfo(tipoInfos){
    
    tipoInfos.forEach(tipoInfo => {
        let option = document.createElement("option");
    option.value = tipoInfo.nome;
    option.innerHTML = tipoInfo.nome;

    comboTipoInfo.appendChild(option);
    });
}

function enviaCadastro(){
    var areaInformacao = new Object();
    areaInformacao.nomeArea = comboArea.value;
    areaInformacao.nomeInformacao = comboTipoInfo.value;
    areaInformacao.descricaoInformacao = descricao.value;


	 fetch(urlAreaInfoApi, 
			 {
		 		method: 'POST',  
		 		headers: {
	 		      'Content-type': 'application/json'
	 		    },
		 		body: JSON.stringify(areaInformacao)
	 		})
	 .then(extrairJSON)
     .then(tratarResposta);
}

function tratarResposta(resposta){
    alert('Sucesso!');
    comboArea.value = "0";
    comboTipoInfo.value = "0";
    descricao.value = "";
}

function extrairJSON(resposta){
    return resposta.json();
}

carregaArea();
carregaTipoInfo();


boatoEnviar.onclick = enviaCadastro;