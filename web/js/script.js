const urlBotApi = "http://localhost:8080/chat";
const inputMensagem = document.querySelector(".message_input");
const boatoEnviar = document.querySelector(".send_message");
const template = document.querySelector(".message_template");
const conversa = document.querySelector(".messages");
let ladoMensagem = 'right';

function checkEnter(e){
	if (e.which === 13) {
		preparaMensagem();
    }
}

function preparaMensagem(){
	let texto = inputMensagem.value;
    enviarMensagem(texto);
    if(!checkEasterEgg(texto)){
	    mensagemChat(texto);
    }
}

function checkEasterEgg(texto){
    switch(texto){
        case "qual o sentido da vida, universo e tudo mais?":
            enviarMensagem("42")
            return true;
        case "hasta la vista baby":
            enviarMensagem("I'll be back")
            return true;
        case "sudo su":
            inputMensagem.placeholder = "#";
            enviarMensagem("Modo super usuário On")
            return true;  
        case "exit":
            inputMensagem.placeholder = "Escreva sua mensagem aqui...";
            enviarMensagem("Modo super usuário Off")
            return true;            
        default:
            return false;
    }
}

function enviarMensagem(texto){
	ladoMensagem = ladoMensagem === 'left' ? 'right' : 'left';
    
    let novaMensagem = criarNovaMensagem(texto);
    novaMensagem.className = "message " + ladoMensagem + " appeared";
    
    conversa.appendChild(novaMensagem);

    conversa.scrollTop += 300;
    inputMensagem.value = "";
}

function criarNovaMensagem(textoMensagem){
    let corpo = document.createElement("li");
    corpo.className = "message";

    let avatar = document.createElement("div");
    avatar.className = "avatar";
  
    let textWraper = document.createElement("div");
    textWraper.className = "text_wrapper"; 
   
    let texto = document.createElement("div");
    texto.className = "text"; 
    texto.innerHTML = textoMensagem;
    
    corpo.appendChild(avatar);
    textWraper.appendChild(texto);
    corpo.appendChild(textWraper);

    return corpo;
}


function mensagemChat(data){
	 fetch(urlBotApi, 
			 {
		 		method: 'POST',  
		 		headers: {
	 		      'Content-type': 'application/json'
	 		    },
		 		body: JSON.stringify(data)
	 		})
	 .then(extrairJSON)
     .then(mensagemChatResposta);
}
function mensagemChatResposta(resposta){
	enviarMensagem(resposta.descricaoInformacao);
}

function extrairJSON(resposta){
    return resposta.json();
}

enviarMensagem("Olá, em que posso ajudar?");


inputMensagem.onkeyup = checkEnter;
boatoEnviar.onclick = preparaMensagem;
