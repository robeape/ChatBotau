const urlAreaApi = "http://localhost:8080/area";
const boatoEnviar = document.querySelector("#enviar");
const nome = document.querySelector("#nome");
const descricao = document.querySelector("#descricao");

function enviaArea(){
    var area = new Object();
    area.nome = nome.value;
    area.descricao = descricao.value;

	 fetch(urlAreaApi, 
			 {
		 		method: 'POST',  
		 		headers: {
	 		      'Content-type': 'application/json'
	 		    },
		 		body: JSON.stringify(area)
	 		})
	 .then(extrairJSON)
     .then(tratarResposta);
}

function tratarResposta(resposta){
    alert('Sucesso!');
    nome.value = "";
    descricao.value = "";
}


function extrairJSON(resposta){
    return resposta.json();
}

boatoEnviar.onclick = enviaArea;