package com.itau.ChatBotau.controllers;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.itau.ChatBotau.controllers.AreaController;
import com.itau.ChatBotau.models.Area;
import com.itau.ChatBotau.repositories.AreaRepository;

@RunWith(SpringRunner.class)
@WebMvcTest(AreaController.class)
public class AreaControllerTest {

	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	AreaRepository areaRepository;
	
	ObjectMapper mapper = new ObjectMapper();
	
	@Test
	public void testarNaoInserirArea() throws Exception {
		Area area = new Area();
		
		area.setNome("RTC");
		area.setDescricao("Descrição da área RTC");
		
		Optional<Area> optional = Optional.of(area);
		
		when(areaRepository.findByNome(any(String.class))).thenReturn(optional);
		
		String json = mapper.writeValueAsString(area);
		
		mockMvc.perform(post("/area")
				.content(json)
				.contentType(MediaType.APPLICATION_JSON))
		.andExpect( status().is(409) );
	
	}
	
	@Test
	public void testarInserirArea() throws Exception {
		Area area = new Area();
		
		area.setNome("RTC");
		area.setDescricao("Descrição da área RTC");
		
		Optional<Area> optional = Optional.empty();
		
		when(areaRepository.findByNome(any(String.class))).thenReturn(optional);
		
		String json = mapper.writeValueAsString(area);
		
		mockMvc.perform(post("/area")
				.content(json)
				.contentType(MediaType.APPLICATION_JSON))
		.andExpect( status().is(200) )
		.andExpect(jsonPath("$.nome", equalTo("RTC")));
	
	}
	
}
