package com.itau.ChatBotau.controllers;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.itau.ChatBotau.models.TipoInformacao;
import com.itau.ChatBotau.repositories.TipoInformacaoRepository;

@RunWith(SpringRunner.class)
@WebMvcTest(TipoInformacaoController.class)
public class TipoInformacaoControllerTest {
	
	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	TipoInformacaoRepository tipoInformacaoRepository;
	
	ObjectMapper mapper = new ObjectMapper();
	
	@Test 
	public void testarNaoInserirTipoInformacao() throws Exception {
		
		TipoInformacao tipoInfo = new TipoInformacao();
		tipoInfo.setNome("TelefoneMockado");
		tipoInfo.setDescricao("Tel de contato mockado.");
		
		Optional<TipoInformacao> optional = Optional.of(tipoInfo);
		
		when (tipoInformacaoRepository.findByNome(any(String.class))).thenReturn(optional);
		
		String json = mapper.writeValueAsString(tipoInfo);
		
		mockMvc.perform(post("/tipoinformacao")
		.content(json)
		.contentType(MediaType.APPLICATION_JSON))
		.andExpect( status().is(409) );
	}
	
	@Test
	public void testarInserirTipoInformacao() throws Exception {
		TipoInformacao tipoInfo = new TipoInformacao();
		
		tipoInfo.setNome("TELEFONE");
		tipoInfo.setDescricao("Telefones úteis");
		
		Optional<TipoInformacao> optional = Optional.empty();
		
		when(tipoInformacaoRepository.findByNome(any(String.class))).thenReturn(optional);
		
		when(tipoInformacaoRepository.save(any(TipoInformacao.class))).thenReturn(tipoInfo);
		
		String json = mapper.writeValueAsString(tipoInfo);
		
		mockMvc.perform(post("/tipoinformacao")
				.content(json)
				.contentType(MediaType.APPLICATION_JSON))
		.andExpect( status().is(200) )
		.andExpect(jsonPath("$.nome", equalTo("TELEFONE")));

	}
	
}
