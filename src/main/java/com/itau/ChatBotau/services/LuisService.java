package com.itau.ChatBotau.services;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.itau.ChatBotau.models.LuisResposta;

@Service
public class LuisService {
	
	private RestTemplate restTemplate = new RestTemplate();
	 
	public LuisResposta getLuisResposta(String query) { 
	
		String url = "https://westus.api.cognitive.microsoft.com/luis/v2.0/apps/7592d3f8-8118-4b20-b9bf-441801f7c931?subscription-key=8833a11e64a5466ba9baa9e9396f1658&verbose=true&timezoneOffset=0&q=";
		url+=query;
		return restTemplate.getForObject(url, LuisResposta.class);
	}

}
