package com.itau.ChatBotau.repositories;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.itau.ChatBotau.models.AreaInformacao;

public interface AreaInformacaoRepository extends CrudRepository<AreaInformacao, String>{

	Optional<AreaInformacao> findByNomeAreaAndNomeInformacao(String nomeArea, String nomeInformacao);


}
