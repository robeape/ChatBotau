package com.itau.ChatBotau.repositories;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.itau.ChatBotau.models.TipoInformacao;

public interface TipoInformacaoRepository extends CrudRepository<TipoInformacao, String>{

	Optional<TipoInformacao> findByNome(String nomeArea);

}
 