package com.itau.ChatBotau.repositories;

import java.util.Optional;
import org.springframework.data.repository.CrudRepository;

import com.itau.ChatBotau.models.Area;

public interface AreaRepository extends CrudRepository<Area, String>{

	Optional<Area> findByNome(String nome);

}
