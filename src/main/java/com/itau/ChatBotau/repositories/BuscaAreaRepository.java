package com.itau.ChatBotau.repositories;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.itau.ChatBotau.models.BuscaArea;

public interface BuscaAreaRepository extends CrudRepository<BuscaArea, String>{

	Optional<Iterable<BuscaArea>> findAllByNomeArea(String nomeArea);
 
}
