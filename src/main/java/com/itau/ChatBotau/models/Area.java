package com.itau.ChatBotau.models;


import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;
import org.springframework.data.cassandra.core.mapping.Table;

@Table
public class Area {

	@PrimaryKeyColumn(type=PrimaryKeyType.PARTITIONED)
	private String nome;
	
	private String descricao;

	public String getNome() {
		return nome.toUpperCase();
	}

	public void setNome(String nomeArea) {
		this.nome = nomeArea.toUpperCase();
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricaoArea) {
		this.descricao = descricaoArea;
	}

}
