package com.itau.ChatBotau.models;

import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;
import org.springframework.data.cassandra.core.mapping.Table;

@Table
public class AreaInformacao {
	 
	@PrimaryKeyColumn(type=PrimaryKeyType.PARTITIONED)
	private String nomeArea;
	
	@PrimaryKeyColumn(type=PrimaryKeyType.CLUSTERED)
	private String nomeInformacao;
	
	private String descricaoInformacao;

	public String getNomeArea() {
		return nomeArea.toUpperCase();
	}

	public void setNomeArea(String nomeArea) {
		this.nomeArea = nomeArea.toUpperCase();
	}

	public String getNomeInformacao() {
		return nomeInformacao.toUpperCase();
	}

	public void setNomeInformacao(String nomeInformacao) {
		this.nomeInformacao = nomeInformacao.toUpperCase();
	}

	public String getDescricaoInformacao() {
		return descricaoInformacao;
	}

	public void setDescricaoInformacao(String descricaoInformacao) {
		this.descricaoInformacao = descricaoInformacao;
	}


}
