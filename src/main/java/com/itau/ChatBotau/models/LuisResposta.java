package com.itau.ChatBotau.models;

import java.util.ArrayList;

public class LuisResposta {
 
	private String query;
	private Intent topScoringIntent;
	private ArrayList<Intent> intenties;
	private ArrayList<Entity> entities;
	public String getQuery() {
		return query;
	}
	public void setQuery(String query) {
		this.query = query;
	}
	public Intent getTopScoringIntent() {
		return topScoringIntent;
	}
	public void setTopScoringIntent(Intent topScoringIntent) {
		this.topScoringIntent = topScoringIntent;
	}
	public ArrayList<Intent> getIntenties() {
		return intenties;
	}
	public void setIntenties(ArrayList<Intent> intenties) {
		this.intenties = intenties;
	}
	public ArrayList<Entity> getEntities() {
		return entities;
	}
	public void setEntities(ArrayList<Entity> entities) {
		this.entities = entities;
	}
	
}

// *** Exempo de resosta do LUIS 
//	{
//	  "query": "quero o telefone do rtc",
//	  "topScoringIntent": {
//	    "intent": "PedidoTelefone",
//	    "score": 0.948929548
//	  },
//	  "intents": [
//	    {
//	      "intent": "PedidoTelefone",
//	      "score": 0.948929548
//	    },
//	    {
//	      "intent": "None",
//	      "score": 0.0162477382
//	    },
//	    {
//	      "intent": "cumprimento",
//	      "score": 0.009810875
//	    }
//	  ],
//	  "entities": [
//	    {
//	      "entity": "rtc",
//	      "type": "Area",
//	      "startIndex": 20,
//	      "endIndex": 22,
//	      "score": 0.945982933
//	    }
//	  ]
//	}	
//
