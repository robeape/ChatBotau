package com.itau.ChatBotau.controllers;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.itau.ChatBotau.models.TipoInformacao;
import com.itau.ChatBotau.repositories.TipoInformacaoRepository;

@RestController
@CrossOrigin
@RequestMapping("/tipoinformacao")
public class TipoInformacaoController {
 
	@Autowired
	TipoInformacaoRepository tipoInformacaoRepository;

	@RequestMapping(method=RequestMethod.GET)
	private Iterable<?> buscar() {
		
		return tipoInformacaoRepository.findAll();
	}
	
	
	@RequestMapping(method=RequestMethod.POST)
	private ResponseEntity<?> criar(@RequestBody TipoInformacao tipoInformacao) {
		Optional<TipoInformacao> optionalTipoInfo = tipoInformacaoRepository.findByNome(tipoInformacao.getNome()); 
		
		if(optionalTipoInfo.isPresent())
			return ResponseEntity.status(409).build();
		else
			return ResponseEntity.ok().body(tipoInformacaoRepository.save(tipoInformacao));
	}
}
