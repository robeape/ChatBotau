package com.itau.ChatBotau.controllers;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.itau.ChatBotau.models.AreaInformacao;
import com.itau.ChatBotau.models.BuscaArea;
import com.itau.ChatBotau.models.LuisResposta;
import com.itau.ChatBotau.repositories.AreaInformacaoRepository;
import com.itau.ChatBotau.repositories.BuscaAreaRepository;
import com.itau.ChatBotau.services.LuisService;

@RestController
@CrossOrigin
public class ChatController {
	
	@Autowired
	LuisService luisService;
	
	@Autowired
	AreaInformacaoRepository areaInformacaoRepository;
	
	@Autowired
	BuscaAreaRepository buscaAreaRepository;
	
	@RequestMapping(method=RequestMethod.POST, path="/chat")
	public ResponseEntity<?> chatBotau(@Valid @RequestBody String mensagem){
		LuisResposta luisResposta = luisService.getLuisResposta(mensagem);
		AreaInformacao respostaPadrao = new AreaInformacao();
		respostaPadrao.setDescricaoInformacao("Desculpe, mas não consigo responder essa pergunta.");
		respostaPadrao.setNomeArea("");
		respostaPadrao.setNomeInformacao("");
		
		try {
			String area = "";
			if(luisResposta.getEntities().size() > 0) {
				area = luisResposta.getEntities().get(0).getEntity().toUpperCase();
			}
			
			String tipoInformacao = "";
			
			if(luisResposta.getTopScoringIntent().getIntent().length() > 0) {
				tipoInformacao = luisResposta.getTopScoringIntent().getIntent().toUpperCase();
			}
			
			if(area.isEmpty() && tipoInformacao.isEmpty()) {
				return ResponseEntity.ok().body(respostaPadrao);
			}
			else
			{
				if(tipoInformacao.equals("geral")) {
					if(!geraRespostaGeralArea(area).isPresent()) {
						return ResponseEntity.ok().body(respostaPadrao);
					}
					else
					{
						return ResponseEntity.ok().body(geraRespostaGeralArea(area).get());
					}
				}else if(tipoInformacao.equals("DESPEDIDA")) {
					respostaPadrao.setDescricaoInformacao("Tamo junto!");

					return ResponseEntity.ok().body(respostaPadrao);
				}
				else
				{
					if(!geraRespostaAreaInformacao(area,tipoInformacao).isPresent() )
						return ResponseEntity.ok().body(respostaPadrao);
					else
						return ResponseEntity.ok().body(geraRespostaAreaInformacao(area,tipoInformacao).get());
				}
			}
		} catch (Exception e) {
			return ResponseEntity.status(503).body(respostaPadrao);
		}
	}
	
	public Optional<Iterable<BuscaArea>> geraRespostaGeralArea(String area) {
		
		Optional<Iterable<BuscaArea>> optionalBuscaArea = buscaAreaRepository.findAllByNomeArea(area);
		
		return optionalBuscaArea;
	}
	
	public Optional<AreaInformacao> geraRespostaAreaInformacao(String area, String tipoInformacao) {
		
		 Optional<AreaInformacao> optionalAreaInformacao = areaInformacaoRepository.findByNomeAreaAndNomeInformacao(area, tipoInformacao);
		 
		 return optionalAreaInformacao;
		
	}
}
