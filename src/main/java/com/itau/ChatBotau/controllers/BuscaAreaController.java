package com.itau.ChatBotau.controllers;

import java.util.Optional;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.itau.ChatBotau.models.BuscaArea;
import com.itau.ChatBotau.repositories.BuscaAreaRepository;

@RestController

public class BuscaAreaController {
 	
	BuscaAreaRepository buscaAreaRepository;

	@RequestMapping("/busca/{nomeArea}")
	public Optional<Iterable<BuscaArea>> buscarInfoPorArea(@PathVariable String nomeArea){
		return buscaAreaRepository.findAllByNomeArea(nomeArea);
	}
	
}
