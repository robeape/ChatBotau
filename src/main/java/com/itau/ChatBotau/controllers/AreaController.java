package com.itau.ChatBotau.controllers;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.itau.ChatBotau.models.Area;
import com.itau.ChatBotau.repositories.AreaRepository;

@RestController
@CrossOrigin
@RequestMapping("/area")
public class AreaController {
	
	@Autowired
	AreaRepository areaRepository;
	
	@RequestMapping(method=RequestMethod.GET)
	public Iterable<?> buscar() {
		return areaRepository.findAll();
	}
	 
	@RequestMapping(method=RequestMethod.POST)
	public ResponseEntity<?> criar(@RequestBody Area area) {

		Optional<Area> optionalArea = areaRepository.findByNome(area.getNome());
		
		if(!optionalArea.isPresent()) {
			area.setNome(area.getNome().toUpperCase());
			areaRepository.save(area);
			return ResponseEntity.ok().body(area);
		}
		return ResponseEntity.status(409).build();
	}
	
	
	
}
