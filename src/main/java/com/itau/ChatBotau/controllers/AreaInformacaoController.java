package com.itau.ChatBotau.controllers;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.itau.ChatBotau.models.AreaInformacao;
import com.itau.ChatBotau.models.BuscaArea;
import com.itau.ChatBotau.repositories.AreaInformacaoRepository;
import com.itau.ChatBotau.repositories.BuscaAreaRepository;

@RestController
@CrossOrigin
@RequestMapping("/areainformacao")
public class AreaInformacaoController {
	
	@Autowired
	AreaInformacaoRepository areaInformacaoRepository;
	
	@Autowired
	BuscaAreaRepository buscaAreaRepository;
	
	@RequestMapping(method=RequestMethod.GET)
	private Iterable<?> buscar () {
		
		return areaInformacaoRepository.findAll();
	}
 
	@RequestMapping(method=RequestMethod.POST)
	private ResponseEntity<?> criar (@RequestBody AreaInformacao areaInformacao) {
		
		Optional<AreaInformacao> optionalAreaInformacao = areaInformacaoRepository.findByNomeAreaAndNomeInformacao(areaInformacao.getNomeArea(), areaInformacao.getNomeInformacao());
		
		if( optionalAreaInformacao.isPresent() )
			return ResponseEntity.status(409).build();
		else
		{
			areaInformacao.setDescricaoInformacao(areaInformacao.getDescricaoInformacao().replaceAll("\n", "<br>"));
			
			BuscaArea buscaArea = new BuscaArea();
			buscaArea.setNomeArea(areaInformacao.getNomeArea());
			buscaArea.setNomeInformacao(areaInformacao.getNomeInformacao());
			buscaArea.setDescricaoInformacao(areaInformacao.getDescricaoInformacao());
			buscaAreaRepository.save(buscaArea);
			
			return ResponseEntity.ok().body(areaInformacaoRepository.save(areaInformacao));
		}
	}
	
}
